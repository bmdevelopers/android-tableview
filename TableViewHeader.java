package co.bytemark.android.opentools.tableview;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import co.bytemark.android.opentools.autoresizer.AutoResizer;

public class TableViewHeader extends LinearLayout 
{

	public Context context;
	public TableView tableView;
	public RelativeLayout m_llContentView;

	public static final int m_nHeaderWidth = AutoResizer.m_nCurrScreenWidth;
	public static final int m_nPaddingLeftRight = AutoResizer.resizeRelativeToOriginalWidth(10);
	
	public static final int[] m_arrnGroupedColors = new int[] {Color.TRANSPARENT, Color.TRANSPARENT};	
	public static final int m_nGroupedHeaderHeight = 27;
	public static final int m_nGroupedHeaderTopPadding = 7;
	public static final int m_nGroupedHeaderBottomPadding = 3;
	
	
	public static final int[] m_arrnPlainColors = new int[] {Color.parseColor("#FF9CA4AA"), Color.parseColor("#FFBCC2C8")};	
	public static final int m_nPlainHeight = 22;
	public static final int m_nPlainTopPadding = 0;
	public static final int m_nPlainBottomPadding = 4;
	

	private TextView m_tvTitleView;
	
	private boolean m_bHidden = true;
	private static final int HIDDEN_HEIGHT = 0;

	
	private TableViewHeader (Context context)
	{
		super (context);
	}
	
	public TableViewHeader(Context context, TableView tableView) 
	{
		this(context, tableView, false);
	}
	

	public TableViewHeader(Context context, TableView tableView, boolean hidden )
	{
		super(context);
		this.context = context;
		this.tableView = tableView;

		setHidden(hidden);		
	}
	
	/**
	 * @category Set_Title
	 * 
	 */

	public TextView getTitleView() 
	{
		if (m_tvTitleView==null){
			setShown();
			m_llContentView.addView(m_tvTitleView = newTitleView());	
		}
		return m_tvTitleView;
	}

	public void setTitle(String title) 
	{
		if (title!=null){
			if (m_tvTitleView==null){
				setShown();
				m_llContentView.addView(m_tvTitleView = newTitleView());
			}
			
			m_tvTitleView.setText(title);
		}
	}
	
	
	public void setTitleTextSize (float _fFontSize)
	{
		if (m_tvTitleView!=null){
			m_tvTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, _fFontSize);
		}
	}
	
	
	/**
	 * @category Set_Interface
	 */
	
	private TextView newTitleView(){
		
		float _fFontSize = 0.0f;
		
		TextView _tvTempTitleView = new TextView(context);
		RelativeLayout.LayoutParams titleViewParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		_tvTempTitleView.setClickable(false);
		

		if (tableView.styleIsPlain()) {
			_tvTempTitleView.setShadowLayer(1, 0, 1, Color.DKGRAY);
			_tvTempTitleView.setTextColor(Color.WHITE);
			_tvTempTitleView.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
			titleViewParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			_tvTempTitleView.setTypeface(null, Typeface.BOLD);
			 _fFontSize = (float)(m_nPlainHeight -  (m_nPlainTopPadding+m_nPlainBottomPadding))-3;
			
		}else if (tableView.styleIsGrouped()){
			_tvTempTitleView.setShadowLayer(1, 0, 1, Color.WHITE);
			_tvTempTitleView.setTextColor(Color.DKGRAY);
			_tvTempTitleView.setGravity(Gravity.LEFT);
			titleViewParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			_tvTempTitleView.setTypeface(null, Typeface.BOLD);
			_fFontSize = (float)(m_nGroupedHeaderHeight -  (m_nGroupedHeaderTopPadding+m_nGroupedHeaderBottomPadding))-2;
		}
		
		_tvTempTitleView.setLayoutParams(titleViewParams);
		int _nFontSizePlain = AutoResizer.resizeRelativeToOriginalWidth(_fFontSize);
		_tvTempTitleView.setTextSize(TypedValue.COMPLEX_UNIT_PX, _nFontSizePlain);
		
		return _tvTempTitleView;
	}
	
	
	/**
	 * @category Hiding_And_Displaying
	 */
	
	public void setHidden (boolean hidden)
	{
		if (hidden){
			setHidden();
		}else{
			setShown();
		}
	}
	
	public void setHidden ()
	{
		m_bHidden = true;
		this.setVisibility(GONE);
		hide ();
	}
	
	private void hide ()
	{
		this.setLayoutParams(new AbsListView.LayoutParams(m_nHeaderWidth, HIDDEN_HEIGHT));
		this.setPadding(m_nPaddingLeftRight, HIDDEN_HEIGHT, m_nPaddingLeftRight, HIDDEN_HEIGHT);
	}
	
	public void setShown ()
	{
		m_bHidden = false;
		this.setVisibility(VISIBLE);
		display ();
	}
	
	@SuppressWarnings("deprecation")
	private void display ()
	{
		int _nHeight = 0;
		int _nTopPadding = 0;
		int _nBottomPadding = 0;
		int _arrnBackgroundColor[] = null;
		
		if (m_llContentView==null){
			m_llContentView = new RelativeLayout (context);
			RelativeLayout.LayoutParams _lpContentViewParams = new RelativeLayout.LayoutParams (LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			m_llContentView.setLayoutParams(_lpContentViewParams);
			m_llContentView.setGravity(Gravity.CENTER_VERTICAL);
			m_llContentView.setBackgroundColor(Color.TRANSPARENT);
			this.addView(m_llContentView);
		}
		
		if (tableView.styleIsPlain()) {
			_nHeight = m_nPlainHeight;
			_nTopPadding = m_nPlainTopPadding;
			_nBottomPadding = m_nPlainBottomPadding;
			_arrnBackgroundColor =  m_arrnPlainColors;
		}else if (tableView.styleIsGrouped()){
			_nHeight = m_nGroupedHeaderHeight;
			_nTopPadding = m_nGroupedHeaderTopPadding;
			_nBottomPadding = m_nGroupedHeaderBottomPadding;
			_arrnBackgroundColor =  m_arrnGroupedColors;

		}
		
		_nHeight = AutoResizer.resizeRelativeToOriginalWidth (_nHeight);
		_nTopPadding = AutoResizer.resizeRelativeToOriginalWidth (_nTopPadding);
		_nBottomPadding = AutoResizer.resizeRelativeToOriginalWidth (_nBottomPadding);

		this.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, _nHeight));
		this.setPadding(m_nPaddingLeftRight, _nTopPadding, m_nPaddingLeftRight, _nBottomPadding);
		GradientDrawable gd = new GradientDrawable(
	            GradientDrawable.Orientation.TOP_BOTTOM,
	            _arrnBackgroundColor);
		
		this.setBackgroundDrawable(gd);
	}
	
	public boolean isHidden ()
	{
		return m_bHidden;
	}

}
